set number
set cursorline
set wrap
set showcmd
" wildmenu 打开vim tab提示
set wildmenu

" 使用前需要安装xclip
set clipboard=unnamedplus

" 搜索时忽略大小写
set ignorecase
" 智能搜索, 大小写
set smartcase
set smartindent
set smarttab
set autoindent
set ignorecase
set hlsearch " 搜索高亮
set incsearch " 搜索实时高亮
set showmode
set autowriteall
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
set ts=4
set shiftwidth=4
set mouse=a
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set list
set autochdir
set scrolloff=5
let mapleader=" "

noremap <c-j> 5j
noremap <c-k> 5k
noremap <c-h> 5h
noremap <c-l> 5l
noremap <c-s> :w<cr>

map <leader>k <c-w>k
map <leader>j <c-w>j
map <leader>h <c-w>h
map <leader>l <c-w>l

map <up> :res -5<cr>
map <down> :res +5<cr>
map <left> :vertical resize-5<cr>
map <right> :vertical resize+5<cr>

map ntt :NERDTreeToggle<CR>
map te :tabe<cr>
map tn :tabn<cr>
map tp :tabp<cr>
map tc :tabc<cr>

map sv <c-w>v
map sh <c-w>s

map <F5> :call CompileRunGcc()<CR>
func! CompileRunGcc()
	exec "w"
	if &filetype == 'c'
		exec "!g++ % -o %<"
		exec "!./%<"
	elseif &filetype == 'cpp'
		exec "!g++ % -o %<"
		exec "!./%<"
	elseif &filetype == 'java'
		exec "!javac %"
		exec "!java %<"
	elseif &filetype == 'sh'
		:!bash %
	elseif &filetype == 'python'
		exec "!python %"
	elseif &filetype == 'html'
		exec "!firefox % &"
	elseif &filetype == 'go'
	"        exec "!go build %<"
		exec "!go run %"
	elseif &filetype == 'mkd'
		exec "!~/.vim/markdown.pl % > %.html &"
		exec "!firefox %.html &"
	endif
endfunc


" 安装插件
call plug#begin()
" tab键补全功能插件
Plug 'ervandew/supertab'

" Coc 智能补全插件引擎
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" vim-airline 标签栏插件
Plug 'Vim-airline/vim-airline'
" vim-airline 标签栏插件的主题插件
Plug 'Vim-airline/vim-airline-themes'

" vim-startify 插件
Plug 'mhinz/vim-startify'
" vim-snazzy 主题插件
Plug 'connorholyday/vim-snazzy'

" markdown 预览插件
" Plug 'iamcco/markdown-preview.nvim'
Plug 'iamcco/markdown-preview.nvim'

" 目录数
Plug 'preservim/nerdtree'

" 括号
Plug 'jiangmiao/auto-pairs'
call plug#end()

" let g:loaded_python3_provider = 0

let g:coc_global_extensions = ['coc-json', 'coc-html', 'coc-css', 'coc-git', 'coc-highlight', 'coc-python', 'coc-sh', 'coc-snippets', 'coc-vimlsp', 'coc-yaml', 'coc-xml', 'coc-markdownlint', 'coc-go']

let g:airline#extensions#tabline#enabled = 1

colorscheme snazzy
let g:SnazzyTransparent = 1

" 指定浏览器路径
" let g:mkdp_path_to_chrome = 'C:\Program Files\Google\Chrome\Application'
" 指定预览主题，默认Github
let g:mkdp_markdown_css=''
